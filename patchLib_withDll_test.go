// -build test
/*package libs

import "C"
import (
	"syscall"
	"unsafe"
	"os"
	"encoding/hex"
	"hash/crc32"
	"io"
)

var patchLib, _        = syscall.LoadLibrary("C:/Users/lgiuliani/Projects/Patcher/dist/lib/patchLib.dll")
var generatePatch, _ = syscall.GetProcAddress(patchLib, "GeneratePatch")
var applyPatch, _ = syscall.GetProcAddress(patchLib, "ApplyPatch")

func Generate (old, new, patch string) int {

	var nargs uintptr = 3
	res := -1
	oldb := append([]byte(old))
	newb := append([]byte(new))
	patchb := append([]byte(patch))	

	if ret, _, callErr := syscall.Syscall9(uintptr(generatePatch), nargs, uintptr(unsafe.Pointer(&oldb[0])), uintptr(unsafe.Pointer(&newb[0])), uintptr(unsafe.Pointer(&patchb[0])), 0, 0, 0, 0, 0, 0); callErr != 0 {	return -1
	} else {
		res = int(ret)
	}
	return res
}

/*func Apply (old, patch, patched string) int {
	var nargs uintptr = 3
	res := -1
	oldb := append([]byte(old), 0)
	patchb := append([]byte(patch), 0)
	patchedb := append([]byte(patched), 0)
	
	if ret, _, callErr := syscall.Syscall(uintptr(generatePatch), nargs, uintptr(unsafe.Pointer(&oldb[0])), uintptr(unsafe.Pointer(&patchedb[0])), uintptr(unsafe.Pointer(&patchb[0]))); callErr != 0 {
		return -1
	} else {
		res = int(ret)
	}
	return res
}*/


/*
func ComputeCrc32(filePath string) (string, error) {
	var returnCRC32String string
	
	var polynomial uint32 = 0xedb88320;
	
	file, err := os.Open(filePath)
	if err != nil {
		return returnCRC32String, err
	}
	defer file.Close()
	tablePolynomial := crc32.MakeTable(polynomial)
	hash := crc32.New(tablePolynomial)
	if _, err := io.Copy(hash, file); err != nil {
		return returnCRC32String, err
	}
	hashInBytes := hash.Sum(nil)[:]
	returnCRC32String = hex.EncodeToString(hashInBytes)
	return returnCRC32String, nil
}

*/