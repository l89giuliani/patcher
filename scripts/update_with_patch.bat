@echo off
SET serviceName=Tomcat8 
SET ftpServer=88.59.60.181
::SET ftpServer=172.16.0.10

cd C:\fer_scripts

::preparing ftp commands
echo open %ftpServer%> ftp_fer.ftp
echo serverftp>> ftp_fer.ftp
echo etg123srl>> ftp_fer.ftp
echo cd ferjava>> ftp_fer.ftp
echo get fer.patch>> ftp_fer.ftp
echo get fer-crc32.txt>> ftp_fer.ftp
echo cd %ComputerName%>> ftp_fer.ftp
echo get application.properties>> ftp_fer.ftp
echo get sql_commands.sql>> ftp_fer.ftp
echo quit>> ftp_fer.ftp

ftp -s:ftp_fer.ftp

copy "C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\webapps\\fer-0.1.0.war" C:\fer_scripts\

patcher apply-crc32 C:\fer_scripts\fer-0.1.0.war C:\fer_scripts\fer.patch C:\fer_scripts\fer-0.1.0_patched.war C:\fer_scripts\fer-crc32.txt > patch_res.txt

set patch_output=""
FOR /F "tokens=*" %%Z IN (patch_res.txt) DO (
set patch_output="%%Z"
)

Echo.%patch_output% | findstr /C:"Success">nul && (
    
	net stop %serviceName%

	copy fer-0.1.0_patched.war "C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\webapps\\fer-0.1.0.war"
	copy application.properties "C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\lib\\"

	set PGPASSWORD=ETG123srl!
	"C:\Program Files\PostgreSQL\10\bin\psql" -U etg -d etg -awf sql_commands.sql

	net start %serviceName%
	echo "success"
	
) || (
    echo "Update Failed"
)

del patch_res.txt




if exist FER_GUI.zip (
	"C:\\Program Files\\7-Zip\\7z.exe" x FER_GUI.zip -aoa
	Xcopy /E /I /y FER_GUI C:\FER_GUI 
)

del application.properties
del sql_commands.sql
del fer-0.1.0.war
del fer-0.1.0_patched.war
del fer.patch
del fer-crc32.txt

pause