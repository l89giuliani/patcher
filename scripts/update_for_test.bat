@echo off
::for /f %%i in (`./patcher.exe apply-crc32 C:\fer_scripts\fer-0.1.0.war C:\fer_scripts\fer.patch C:\fer_scripts\fer-0.1.0_patched.war C:\fer_scripts\fer-crc32.txt`) do set res=%%i


::patcher.exe generate-crc32 C:/patchTests/fer-0.1.0.war C:/patchTests/fer-0.1.0_new.war C:/patchTests/fer.patch C:/patchTests/fer-crc32.txt > patch_res.txt

patcher.exe apply-crc32 C:/patchTests/fer-0.1.0.war C:/patchTests/fer.patch C:/patchTests/patched_fer.war C:/patchTests/fer-crc32.txt > patch_res.txt

set patch_output=""
FOR /F "tokens=*" %%Z IN (patch_res.txt) DO (
set patch_output="%%Z"
)

Echo.%patch_output% | findstr /C:"Success">nul && (
    echo "true"
) || (
    echo "false"
)

del patch_res.txt